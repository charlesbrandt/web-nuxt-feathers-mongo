# About boilerplate

High level overview for a website.

Consider keeping your content in a separate git based repository from the code.

## Reminder

Currently @nuxt/content expects one file to be in the root of the `content/` directory. You can delete this one after adding other content as necessary for your application.


## Content

https://tailwindcss.com/docs/font-size
Font Size - Tailwind CSS
https://nuxtjs.org/blog/creating-blog-with-nuxt-content#add-a-search-field
Create a Blog with Nuxt Content - NuxtJS
