---
title: Nuxt Content Guide
description: Learn how to use @nuxt/content.
---

# Nuxt Content

## Writing Content

https://content.nuxtjs.org/writing/

### Excerpts

The `<!--more-->` tag in your markdown allows you to specify where you want the summary to break. Nice!

### Variables

These variables will be injected into the document automatically:

```
{
  body: Object
  excerpt: Object
  dir: "/"
  extension: ".md"
  path: "/index"
  slug: "index"
  toc: Array
  createdAt: DateTime
  updatedAt: DateTime
}
```

### Front Matter (YAML Blocks)

Include any valid yaml in the section at the top that starts with `---` and ends with `---`. Good for variables and arrays.

#### Date

model:
dateTimeField: 2010-04-16 16:32:03 -5

https://yaml.org/type/timestamp.html

https://stackoverflow.com/questions/2656196/ruby-how-do-i-define-a-datetime-field-in-yaml

#### Array

An array can be represented as: `['key1', 'key2', 'key3']`.

An array can also be written spanning multiple lines where each line starts with a dash followed by a space and then a string:

```
- string1
- string2
- string3
```

This evaluates to: `['string1', 'string2', 'string3']`

#### Map / hash / dictonary / object

```
key1: string1
key2: string2
key3: string3
```

Maps are the base of how YAML gets parsed.

#### Strings

https://yaml-multiline.info/

#### Case conventions for variable names

camelCase is used heavily in Javascript.

## Fetching Content

https://content.nuxtjs.org/fetching/

## Links

https://nuxtjs.org/blog/creating-blog-with-nuxt-content#live-editing-our-content
