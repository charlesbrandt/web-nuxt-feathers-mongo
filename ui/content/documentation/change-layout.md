## Update default template

    cd boilerplate/ui/layouts
    ls -l

Notice that default.vue should be a link to the one you want by default.

To update

    rm default.vue
    ln -s minimal.vue default.vue

I think it's still a good idea to have the layout set explicitly in ui/pages/ components.

```
<script>
export default {
  layout: 'minimal',
  data() {
  }
  ...
```
