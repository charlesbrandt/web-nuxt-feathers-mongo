---
photo: person-name.jpg
aka: ['list-of', 'alternate-ids']
active: true
joinedAt: 2021-01-01
title: Developer
role: 'primary-role'
roles: ['if-more-than-one-roll', 'optional']
groups:
  - group1
  - group2
url: https://example.com/person/person-name
---

# Person Name

Person description

<!--more-->

Even more details

## Settings

`photo` will be relative to `/static` directory

## Optional Settings

`id: person-name`

defaults to slug if none or missing
optionally allows override default if file names and id schemes do not match (not recommended)

`name: Person Name`

Use H1 by default. Can over-ride if necessary. (not recommended)

## Custom Settings

Different contexts will require tracking additional settings.

Those can be added as needed.

Only track the information you need.
