const { Builder, By, Key, until } = require("selenium-webdriver");

(async function example() {
  let driver = await new Builder().forBrowser("chrome").build();
  try {
    // This should track whatever you've got your docker containers set up as
    await driver.get("http://boilerplate_ui_1:3000");
    // await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    // await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
  } finally {
    // if you want to keep the browser open
    // await driver.quit();
  }
})();
